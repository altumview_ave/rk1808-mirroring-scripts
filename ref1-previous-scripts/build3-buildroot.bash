#!/bin/bash

if [ ! -d ../repo ]; then
    echo Error: no ../repo directory
    exit 1
fi

echo ; echo Building buildroot ... ; echo

echo 'Run   : ' source buildroot/build/envsetup.sh
echo 'Then  : ' make

echo

echo Choose from: 
echo '    ' 16. rockchip_rk1808
echo '    ' 17. rockchip_rk1808_ai_camera
echo '    ' 18. rockchip_rk1808_compute_stick
echo '    ' 19. rockchip_rk1808-multi
echo


