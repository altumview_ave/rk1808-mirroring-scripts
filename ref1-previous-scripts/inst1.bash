#!/bin/bash

sudo apt-get install \
    git-core gitk git-gui gcc-arm-linux-gnueabihf u-boot-tools \
    device-tree-compiler gcc-aarch64-linux-gnu mtools parted libudev-dev libusb-1.0-0-dev \
    python-linaro-image-tools linaro-image-tools autoconf autotools-dev libsigsegv2 m4 \
    intltool libdrm-dev \
    curl sed \
    make binutils build-essential gcc g++ bash patch gzip bzip2 perl tar cpio python unzip \
    rsync file \
    bc wget libncurses5 libqt4-dev libglib2.0-dev libgtk2.0-dev libglade2-dev cvs git \
    mercurial rsync openssh-client subversion asciidoc w3m dblatex graphviz \
    python-matplotlib libc6:i386 liblz4-tool \
    gawk


# repo: removed to use a local copy
# gawk: needed to build u-boot image. 


