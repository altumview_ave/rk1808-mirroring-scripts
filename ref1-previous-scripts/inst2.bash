#!/bin/bash

if [ -d repo -o -d linux ]; then
    echo Error: existing repo/ or linux/ 
    exit 1
fi

git clone https://github.com/rockchip-linux/repo
mkdir linux
cd linux


