#!/bin/bash

if [ ! -d sharedfiles ]; then mkdir sharedfiles; fi
if [ ! -d buildfiles ]; then mkdir buildfiles; fi
if [ ! -d rkbuildroot ]; then mkdir rkbuildroot; fi

    docker run -td \
         -v $(pwd)/sharedfiles:/home/rk1808user/sharedfiles \
         -v $(pwd)/buildfiles:/home/rk1808user/buildfiles   \
         -v $(pwd)/rkbuildroot:/home/rk1808user/rkbuildroot   \
         --name rkbuild  rkimg


