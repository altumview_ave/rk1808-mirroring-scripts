#!/bin/bash

if [ ! -d ../repo ]; then
    echo Error: no ../repo directory
    exit 1
fi

tm1=$(date +%s)

echo ; echo Building kernel ... ; echo

cd kernel
make rk1808_linux_defconfig
make rk1808-evb-v10.img

echo ; echo Building kernel ... done ; echo

tm2=$(date +%s)
tdif=$(( $tm2 - $tm1 ))
echo time used $tdif seconds
echo

