#!/bin/bash
# bb.bash

# current uid and gid 
curr_uid=`id -u` 
curr_gid=`id -g` 

echo Creating bb.dockerfile ...

# create bb.dockerfile:
cat << EOF1 > bb.dockerfile

 FROM ubuntu:bionic-20200630

 RUN apt-get update
 RUN apt-get install -y git tree openssh-client make
 RUN apt-get install -y bzip2 gcc libncurses5-dev bc
 RUN apt-get install -y file vim
 RUN apt-get install -y zlib1g-dev g++
 RUN apt-get install -y libssl-dev

 # from "1.2.3 Installing the software package" of "Description of the installation 
 # and upgrade of the Hi3559A/C V100 SDK.pdf", Issue 00B03 (2018-04-04)
 #RUN apt-get install -y make zlib1g-dev libncurses5-dev g++ bc libssl-dev 
 RUN apt-get install -y lib32z1 lib32stdc++6  ncurses-term libncursesw5-dev
 RUN apt-get install -y texinfo texlive gawk 

 ## RUN dpkg --add-architecture i386
 ## RUN apt-get update
 ## RUN apt-get install -y libc6:i386 libncurses5:i386 libstdc++6:i386 zlib1g:i386
 # from "1.2.3 Installing the software package"
 #RUN apt-get install -y libc6:i386 
 ## RUN apt-get install -y u-boot-tools:i386

 # needed by wl18xx build
 RUN apt-get install -y autoconf libtool libglib2.0-dev bison flex

 # rk1808 96boards-tb-96aiot dependencies:
 RUN dpkg --add-architecture i386
 RUN apt-get update
 RUN DEBIAN_FRONTEND="noninteractive" TZ="America/Los_Angeles" apt-get install -y \
    git-core gitk git-gui gcc-arm-linux-gnueabihf u-boot-tools \
    device-tree-compiler gcc-aarch64-linux-gnu mtools parted libudev-dev libusb-1.0-0-dev \
    python-linaro-image-tools linaro-image-tools autoconf autotools-dev libsigsegv2 m4 \
    intltool libdrm-dev \
    curl sed \
    make binutils build-essential gcc g++ bash patch gzip bzip2 perl tar cpio python unzip \
    rsync file \
    bc wget libncurses5 libqt4-dev libglib2.0-dev libgtk2.0-dev libglade2-dev cvs git \
    mercurial rsync openssh-client subversion asciidoc w3m dblatex graphviz \
    python-matplotlib libc6:i386 liblz4-tool \
    gawk

  # repo: removed to use a local copy
  # gawk: needed to build u-boot image.

 # time needed for the rk1808 recovery build.sh script:
 RUN apt-get install -y time

 ARG UNAME=rk1808user

 ARG UID=9999
 ARG GID=9999

 RUN groupadd -g \$GID \$UNAME
 RUN useradd -m -u \$UID -g \$GID -s /bin/bash \$UNAME

 RUN rm /bin/sh && ln -s bash /bin/sh
 RUN cp -a /etc /etc-original && chmod a+rw /etc

 USER \$UNAME

 CMD /bin/bash
EOF1

echo Docker build off bb.dockerfile ...
docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) \
     -f bb.dockerfile  -t rkimg .

echo Docker build finished ...


