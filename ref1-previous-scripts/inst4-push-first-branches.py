#!/usr/bin/env python
# inst4-push-first-branches.py
# ~/rkbuildroot/linux$ ../repo/repo forall -c "python $(pwd)/../inst4-push-first-branches.py"


import subprocess
import os
import sys

repo_name = os.getenv("REPO_PROJECT")
repo_index = os.getenv("REPO_I")
repo_count = os.getenv("REPO_COUNT")
repo_slug = repo_name.lower()

run_dir = __file__
if run_dir.startswith('/'):
    run_dir = os.path.abspath(os.path.dirname(run_dir))
else:
    run_dir = os.path.abspath(os.path.dirname(os.getcwd() + "/" + run_dir))

run_user, run_pass = None, None
if os.path.isfile(run_dir + "/" + "inst4-push-user-pass"):
    with open(run_dir + "/" + "inst4-push-user-pass", "r") as f:
        run_user = f.readline().strip()
        run_pass = f.readline().strip()
        f.close()

print("")
print(" REPO_PROJECT %s %s  %s of %s" % (repo_name, repo_slug, repo_index, repo_count))
print("  cwd: %s" % (os.getcwd()))

if run_user is None or run_pass is None or len(str(run_user)) < 3 or len(str(run_pass)) < 3:
    print("Please create file \"%s\" and put git user and pass in it as first two lines." % (
            run_dir + "/" + "inst4-push-user-pass" ))
    sys.exit(1) # fail

def run_cmd(cmds_in, is_git_push=False):
        cmds_sent = [x for x in cmds_in]
        cmds_show = [x for x in cmds_in]
        if is_git_push:
            cmds_sent.append("https://%s:%s@bitbucket.org/altumview-rk1808/%s.git" % (
                    run_user, run_pass, repo_slug))
            cmds_show.append("https://%d:%d@bitbucket.org/altumview-rk1808/%s.git" % (
                    len(run_user), len(run_pass), repo_slug))

        proc = subprocess.Popen(cmds_sent, shell=False, bufsize=1, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output = []
        while (True):
                # Read line from stdout, break if EOF reached, append line to output
                line = proc.stdout.readline().decode()
                if (line == ""): break
                output.append(line.strip())
        proc.wait()
        rc = proc.returncode

        print("  run_cmd: %s " % ( str(cmds_show) ))
        print("           rc %d output len %d content " % (rc, len(output)))
        for x in output:
                print("        %s" % str(x) )
        return rc, output

rc_all = 0

cmds = ["git", "checkout", "-b", "altumview_1_0720"]
rc, _ = run_cmd(cmds)
##rc_all |= rc

cmds = ["git", "checkout", "altumview_1_0720"]
rc, _ = run_cmd(cmds)
rc_all |= rc

cmds = ["git", "branch", "-av"]
rc, output = run_cmd(cmds)
rc_all |= rc

print("    git branch av command : rc %d output len %d content " % (rc, len(output)))
for x in output:
    segs = str(x).split(' ')
    print("        %s         :: %s" % (str(x), segs[0]) )

# should care about only "remotes/beiqi/branchname" or "remotes/rockchip-linux/branchname":
#
#   ~/rkbuildroot/linux/app/qsetting$ git branch -av
#   * altumview_1_0720     5bc5db6 try to reconnect wifi
#     remotes/beiqi/master 5bc5db6 try to reconnect wifi
#     remotes/m/master     5bc5db6 try to reconnect wifi
#
#   ~/rkbuildroot/linux/external/alsa-config$ git branch -av
#   * altumview_1_0720              566e426 Merge commit '56ad302'
#     remotes/m/master              566e426 Merge commit '56ad302'
#     remotes/rockchip-linux/master 566e426 Merge commit '56ad302'

for x in output:
    segs = str(x).split(' ')
    rcmds = []
    if len(segs) > 1:
        brn = segs[0].split('/')
        if len(brn) == 3:
            if brn[0] == "remotes":
                if brn[1] == "beiqi" or brn[1] == "rockchip-linux":
                    rcmds = [brn[1], brn[2]]
                elif brn[1] != "m":
                    print("    Error: unknown remote \"%s\". not \"beiqi\" or \"rockchip-linux\"." % brn[1])
    if len(rcmds) == 2:

        cmds = [ "git", "checkout", "-b", rcmds[1], "%s/%s" % (rcmds[0], rcmds[1]) ]
        rc, _ = run_cmd(cmds) 
        ##rc_all |= rc

        cmds = [ "git", "checkout", rcmds[1] ]
        rc, _ = run_cmd(cmds) 
        rc_all |= rc
        
cmds = ["git", "checkout", "altumview_1_0720"]
rc, _ = run_cmd(cmds)
rc_all |= rc

cmds = ["git", "push", "--all"] 
rc, _ = run_cmd(cmds, is_git_push=True)
rc_all |= rc

cmds = ["git", "push", "--tags"] 
rc, _ = run_cmd(cmds, is_git_push=True)
rc_all |= rc

print(" rc_all %d" % rc_all)
sys.exit(rc_all)


