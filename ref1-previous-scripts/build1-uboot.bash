#!/bin/bash

if [ ! -d ../repo ]; then
    echo Error: no ../repo directory
    exit 1
fi

tm1=$(date +%s)

echo ; echo Building u-boot ... ; echo

cd u-boot
./make.sh rk1808

echo ; echo Building u-boot ... done ; echo

tm2=$(date +%s)
tdif=$(( $tm2 - $tm1 ))
echo time used $tdif seconds
echo

