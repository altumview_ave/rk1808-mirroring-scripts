#!/bin/bash

if [ ! -d sharedfiles ]; then mkdir sharedfiles; fi
if [ ! -d buildfiles ]; then mkdir buildfiles; fi
if [ ! -d rvbuildroot ]; then mkdir rvbuildroot; fi

    docker run -td \
         -v $(pwd)/sharedfiles:/home/rv1126user/sharedfiles \
         -v $(pwd)/buildfiles:/home/rv1126user/buildfiles   \
         -v $(pwd)/rvbuildroot:/home/rv1126user/rvbuildroot   \
         --name rvbuild  rvimg


