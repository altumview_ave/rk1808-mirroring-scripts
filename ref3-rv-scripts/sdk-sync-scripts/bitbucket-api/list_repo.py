#!/usr/bin/env python

import requests
from requests.auth import HTTPBasicAuth
import os

# config: set user-pass in a settings file:
#     line 1 : username, line 2 : password, line 3 : team default to altumview_rv1126
_config_user_pass_file = "settings-pull-user-pass"

# config: None or "" for no selection. or a string of a project name
_config_select_project = "qy_sdk"

# script dir
run_dir = __file__
if run_dir.startswith('/'):
    run_dir = os.path.abspath(os.path.dirname(run_dir))
else:
    run_dir = os.path.abspath(os.path.dirname(os.getcwd() + "/" + run_dir))

##Login
run_user, run_pass, run_team = None, None, None
if os.path.isfile(run_dir + "/" + _config_user_pass_file):
    with open(run_dir + "/" + _config_user_pass_file, "r") as f:
        run_user = f.readline().strip() # first line contains the user name
        run_pass = f.readline().strip() # second line contains the password
        tmp_team = f.readline().strip() # thrid line optional team name
        if len(tmp_team) > 3 and not tmp_team.startswith('#'):
            run_team = tmp_team
        f.close()
else:
    print("Error: the settings file for user pass does not exist.")

username = run_user
password = run_pass
team = run_team

full_repo_list = []
new_repo_list = []

# Request 10 repositories per page (and only their slugs), and the next page URL
#next_page_url = 'https://api.bitbucket.org/2.0/repositories/%s?pagelen=10&fields=next,values.links.clone.href,values.slug' % team
#next_page_url = 'https://api.bitbucket.org/2.0/repositories/%s?pagelen=10' % team
next_page_url = 'https://api.bitbucket.org/2.0/repositories/%s?pagelen=10&fields=next,values.name,values.slug,values.project.name' % team

# Keep fetching pages while there's a page to fetch
repocnt = 0
errcnt = 0
while next_page_url is not None:
    response = requests.get(next_page_url, auth=HTTPBasicAuth(username, password))
    page_json = response.json()

    # Parse repositories from the JSON
    for repo in page_json['values']:
        repocnt += 1

        projname = repo['project']['name']
        reponame = repo['name']
        reposlug = repo['slug']

        if _config_select_project is not None and len(_config_select_project) > 0:
            if projname != _config_select_project:
                continue # skip

        full_repo_list.append([projname, reponame, reposlug])
        print("%12s : %40s : %40s" % (projname, reponame, reposlug))

        # at most 100 pages for branches link. it should be 1.
        repo_url = 'https://api.bitbucket.org/2.0/repositories/%s/%s%s' % (team, reposlug,
                        "?pagelen=100&fields=next,links.branches.href")
        repo_response = requests.get(repo_url, auth=HTTPBasicAuth(username, password))
        repo_page_json = repo_response.json()

        br_url = repo_page_json['links']['branches']['href']
        br_response = requests.get(br_url +
                                   "?pagelen=100&fields=next,values.name,values.target",
                                   auth=HTTPBasicAuth(username, password))
        br_page_json = br_response.json()
        prev_hash = None
        new_repo_set = False
        for j,x in enumerate(br_page_json['values']):
            br_name = x['name']
            br_hash = x['target']['hash']
            pr_new_ind = " "
            if j != 0 and prev_hash != br_hash:
                pr_new_ind = "*" # new code off master
                if new_repo_set != True:
                    new_repo_set = True
                    new_repo_list.append([projname, reponame, reposlug])

            prev_hash = br_hash

            print("%12s   %40s %s branch : %-6s  hash : %s" % (" ", " ", pr_new_ind, br_name, br_hash))

        br_next_page_url = br_page_json.get('next', None)
        if br_next_page_url is not None:
            print("Error: ", "br_next_page_url is not None")
            errcnt += 1
            break
        repo_next_page_url = repo_page_json.get('next', None)
        if br_next_page_url is not None:
            print("Error: ", "repo_next_page_url is not None")
            errcnt += 1
            break
    if errcnt > 0:
        break

    # Get the next page URL, if present
    # It will include same query parameters, so no need to append them again
    next_page_url = page_json.get('next', None)

# Result length will be equal to `size` returned on any page
print("Result:", len(full_repo_list))

print("Changed repos: ")
for x in new_repo_list:
    projname, reponame, reposlug = x
    print("    " + "%12s : %40s : %40s" % (projname, reponame, reposlug))


# run screenshot:
'''
/home/tuser2/PycharmProjects/untitled1/venv/bin/python /home/tuser2/bin/pycharm-community-2019.3.2/plugins/python-ce/helpers/pydev/pydevd.py --multiproc --qt-support=auto --client 127.0.0.1 --port 38663 --file /home/tuser2/tmprv/working-local-repos/rk1808-mirroring-scripts/ref3-rv-scripts/sdk-sync-scripts/bitbucket-api/list_repo.py
pydev debugger: process 1744064 is connecting

Connected to pydev debugger (build 193.6015.41)
      qy_sdk :        linux/app/CameraFactoryTestServer :        linux-app-camerafactorytestserver
                                                          branch : at      hash : 4bf562db7718264772ed9c52e1afc54ccebc8356
                                                          branch : master  hash : 4bf562db7718264772ed9c52e1afc54ccebc8356
....
      qy_sdk :                                rk/kernel :                                rk-kernel
                                                          branch : at      hash : 7f362f66d768e4364fd8778e3aa3380a25dd55b3
                                                        * branch : master  hash : e291f3f4deb48709cf62448d6ba45d6ea37c16fe
      qy_sdk : rk/prebuilts/gcc-arm-8.3-2019.03-x86_64-arm-linux-gnueabihf : rk-prebuilts-gcc-arm-8.3-2019.03-x86_64-arm-linux-gnueabihf
                                                          branch : at      hash : 27edb819ca803f5da253ccbd4134d6337a7310d1
                                                          branch : master  hash : 27edb819ca803f5da253ccbd4134d6337a7310d1
....
      qy_sdk : rk/prebuilts/gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf : rk-preb-gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf
                                                          branch : at      hash : dd4df7dff4f1df65d78939a958b760a79e3650a1
                                                          branch : master  hash : dd4df7dff4f1df65d78939a958b760a79e3650a1
Result: 70
Changed repos: 
          qy_sdk :                linux/app/network_manager :                linux-app-network_manager
          qy_sdk :                      rk/rk1108/buildroot :                      rk-rk1108-buildroot
          qy_sdk :                    linux/device/rockchip :                    linux-device-rockchip
          qy_sdk :       linux/external/camera_engine_rkaiq :       linux-external-camera_engine_rkaiq
          qy_sdk :                  linux/external/isp2-ipc :                  linux-external-isp2-ipc
          qy_sdk :                                rk/kernel :                                rk-kernel
'''

