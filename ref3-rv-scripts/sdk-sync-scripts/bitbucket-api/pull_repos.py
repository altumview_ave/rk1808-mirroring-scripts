#!/usr/bin/env python

import requests
from requests.auth import HTTPBasicAuth
import os, time

# config: set user-pass in a settings file:
#     line 1 : username, line 2 : password, line 3 : team default to altumview_rv1126
_config_user_pass_file = "settings-pull-user-pass"

# config: repos to pull as output by list_repo.py
_config_pull_repos_file = "settings-pull-repos"

# script dir
run_dir = __file__
if run_dir.startswith('/'):
    run_dir = os.path.abspath(os.path.dirname(run_dir))
else:
    run_dir = os.path.abspath(os.path.dirname(os.getcwd() + "/" + run_dir))

##Login
run_user, run_pass, run_team = None, None, None
if os.path.isfile(run_dir + "/" + _config_user_pass_file):
    with open(run_dir + "/" + _config_user_pass_file, "r") as f:
        run_user = f.readline().strip() # first line contains the user name
        run_pass = f.readline().strip() # second line contains the password
        tmp_team = f.readline().strip() # thrid line optional team name
        if len(tmp_team) > 3 and not tmp_team.startswith('#'):
            run_team = tmp_team
        f.close()
else:
    print("Error: the settings file for user pass does not exist.")

pull_repo_list = []
if os.path.isfile(run_dir + "/" + _config_user_pass_file):
    with open(run_dir + "/" + _config_pull_repos_file, "r") as f:
        while True:
            tmp_repo = f.readline().strip()
            if len(tmp_repo) > 0 and tmp_repo.startswith("#"): # comment line
                continue # skip this line
            if len(tmp_repo) < 1:
                break # done
            #   qy_sdk :   linux/app/network_manager :   linux-app-network_manager
            segs = tmp_repo.split(":")
            if len(segs) != 3:
                continue
            pull_repo_list.append([segs[1].strip(), segs[2].strip()]) # [ repo name, repo slug ]
        f.close()
else:
    print("Error: the settings file for pull repos does not exist.")
print("Repos: ", len(pull_repo_list))

tm0 = time.time()
for x in pull_repo_list:
    tm1 = time.time()
    rname, slug = x
    segs = rname.split("/")
    if len(segs) < 2:
        print("Error: len rname smaller than 2 : ", len(segs), rname)
        break
    dirname = "/".join(segs[:-1])
    reponame = segs[-1]
    cwd = os.getcwd()
    rcs = 0
    if not os.path.isdir(dirname):
        rcs |= os.system("mkdir -p " + dirname)
    os.chdir(dirname)
    if not os.path.isdir(reponame):
        print(" Cloning repo : ", rname)
        cmd = "git clone https://%s:%s@bitbucket.org/%s/%s %s" % (
                    run_user, run_pass, run_team, slug, reponame)
        rcs |= os.system(cmd)
    else:
        print(" Skip existing repo : ", rname)
    print("   Repo .git size: ", end="")
    os.system("du -sh %s/.git/" % reponame)
    os.chdir(cwd)
    tm2 = time.time()
    ok = "OK" if rcs == 0 else "FAILED"
    print("\n Done %s  %s  cost %.2f since %.2f\n" % (str(x), ok, tm2-tm1, tm2-tm0))
    if rcs != 0:
        print("Error: ", "break due to failed command")
        break
