#!/bin/bash
# bb.bash

# current uid and gid 
curr_uid=`id -u` 
curr_gid=`id -g` 

echo Creating bb.dockerfile ...

# create bb.dockerfile:
cat << EOF1 > bb.dockerfile

 FROM ubuntu:focal-20201106

 RUN apt-get update
 RUN apt-get install -y tree 
 RUN apt-get install -y file vim

 # for git from source
 RUN apt-get install build-essential fakeroot dpkg-dev -y

 # deb-src
 RUN sed -i -e 's/^# deb-src/ deb-src/' /etc/apt/sources.list
 RUN apt-get update

 # tzdata
   ## preesed tzdata, update package index, upgrade packages and install needed software
   RUN truncate -s0 /tmp/preseed.cfg && \
       (echo "tzdata tzdata/Areas select America" >> /tmp/preseed.cfg) && \
       (echo "tzdata tzdata/Zones/America select Los_Angeles" >> /tmp/preseed.cfg) && \
       debconf-set-selections /tmp/preseed.cfg && \
       rm -f /etc/timezone /etc/localtime && \
       apt-get update && \
       DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true \
       apt-get install -y tzdata
   ## cleanup of files from setup
   RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
 RUN apt-get update

 # for git from source
 RUN apt-get build-dep git -y
 RUN apt-get install libcurl4-openssl-dev -y

 RUN cd /root && mkdir git-source && cd git-source && apt-get source git && \
     tar xf git_2.25.1.orig.tar.xz && cd git-2.25.1 && \
     tar xf ../git_2.25.1-1ubuntu3.debian.tar.xz
 RUN cd /root/git-source/git-2.25.1 && \
     sed -i -- 's/libcurl4-gnutls-dev/libcurl4-openssl-dev/' ./debian/control && \
     sed -i -- '/TEST\s*=\s*test/d' ./debian/rules
 RUN cd /root/git-source/git-2.25.1 && \
     dpkg-buildpackage -rfakeroot -b -uc -us

 # for git from source
 RUN apt-get install -y git-man
 RUN cd /root/git-source && (dpkg -i git_*ubuntu*.deb || echo ' 'OK ignore errors)

 # for missing git-man:
 RUN apt-get --fix-broken install -y
 RUN apt-get autoremove -y

 # for git-repo
 RUN apt-get install -y python3
 RUN apt-get install -y less


 ARG UNAME=rv11user

 ARG UID=9999
 ARG GID=9999

 RUN groupadd -g \$GID \$UNAME
 RUN useradd -m -u \$UID -g \$GID -s /bin/bash \$UNAME

 RUN rm /bin/sh && ln -s bash /bin/sh
 RUN cp -a /etc /etc-original && chmod a+rw /etc

 USER \$UNAME

 CMD /bin/bash
EOF1

echo Docker build off bb.dockerfile ...
docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) \
     -f bb.dockerfile  -t rv11img .

echo Docker build finished ...


