#!/bin/bash

if [ ! -d sharedfiles ]; then mkdir sharedfiles; fi
if [ ! -d buildfiles ]; then mkdir buildfiles; fi
if [ ! -d rvbuildroot ]; then mkdir rvbuildroot; fi

    docker run -td \
         -v $(pwd)/sharedfiles:/home/rv11user/sharedfiles \
         -v $(pwd)/buildfiles:/home/rv11user/buildfiles   \
         -v $(pwd)/rvbuildroot:/home/rv11user/rvbuildroot   \
         --name rv11build  rv11img


