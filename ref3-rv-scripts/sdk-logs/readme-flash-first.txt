
rk-dev-tool 2.74

  [1] start the tool. found an adb device. 
  [2] click "switch". found a loader device.
  [3] click "device partition table:. read in informtion pane: 
        Partition is gpt
        NO  LBA        Size       Name
        01  0x00004000 0x00002000 uboot
        02  0x00006000 0x00002000 misc
        03  0x00008000 0x00010000 boot
        04  0x00018000 0x00010000 recovery
        05  0x00028000 0x00010000 backup
        06  0x00038000 0x00200000 rootfs
        07  0x00238000 0x00040000 oem
        08  0x00278000 0x037c5fdf userdata

parameter.txt in .../IMAGES/
    CMDLINE: mtdparts=rk29xxnand:0x00002000@0x00004000(uboot),
                                 0x00002000@0x00006000(misc),
                                 0x00010000@0x00008000(boot),
                                 0x00010000@0x00018000(recovery),
                                 0x00010000@0x00028000(backup),
                                 0x00200000@0x00038000(rootfs),
                                 0x00040000@0x00238000(oem),
                                 -@0x00278000(userdata:grow)

image download: 
      #     address    name      path
      1     0x00000000 Loader    .../MiniLoaderAll.bin
      2     0x00000000 Parameter .../parameter.txt
      3 y   0x00004000 Uboot     .../uboot.img
      4 y   0x00006000 Misc      .../misc.img
      5 y   0x00008000 Boot      .../boot.img
      6 y   0x00018000 Recovery  .../recovery.img
      7 y   0x00038000 System    .../rootfs.img
      8 y   0x00238000 Oem       .../oem.img

  click "run". 
  right click, ckick "export configuration". save to "saved-config-1". 

  also flash partition 1 and 2. ok. 

