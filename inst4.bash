#!/bin/bash

if [ ! -d ../repo ]; then
    echo Error: no ../repo directory
    exit 1
fi

if [ -d .repo ]; then 
    echo Error: ./repo directory already created
    exit 1
fi

mkdir .repo
ln -s ../../repo .repo/repo

##../repo/repo init --repo-url http://github.com/aosp-mirror/tools_repo.git \
##        init -u https://github.com/96boards-tb-96aiot/manifest.git

../repo/repo init --repo-url ../repo \
	init -u https://bitbucket.org/altumview-rk1808/https-github.com-96boards-tb-96aiot-manifest.git

echo ; echo repo sync ... ; echo
tm1=$(date +%s)

../repo/repo sync

echo ; echo repo sync ... done ; echo
tm2=$(date +%s)
tdif=$(( $tm2 - $tm1 ))
echo time used $tdif seconds
echo

